<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soal2a</title>
</head>

<body>
    <?php if (empty($_GET['step'])) : ?>
        <form action="/soal-a/soal2a.php?step=2" method="POST">
            <div style="display: flex;">
                <label for="">Nama Anda : </label>
                <input type="text" name="name">
            </div>
            <button type="submit">Submit</button>
        </form>
    <?php endif; ?>

    <?php if (!empty($_GET['step'])) : ?>
        <?php if ($_GET['step'] == '2') : ?>
            <form action="/soal-a/soal2a.php?step=3" method="POST">
                <div style="display: flex;">
                    <label for="">Umur Anda : </label>
                    <input type="number" name="old">
                    <input type="hidden" name="name" value="<?= $_POST['name'] ?? '' ?>">
                </div>
                <button type="submit">Submit</button>
            </form>
        <?php endif; ?>
        <?php if ($_GET['step'] == '3') : ?>
            <form action="/soal-a/soal2a.php?step=4" method="POST">
                <div style="display: flex;">
                    <label for="">Hobi Anda : </label>
                    <input type="text" name="hobby">
                    <input type="hidden" name="old" value="<?= $_POST['old'] ?? '' ?>">
                    <input type="hidden" name="name" value="<?= $_POST['name'] ?? '' ?>">
                </div>
                <button type="submit">Submit</button>
            </form>
        <?php endif; ?>
        <?php if ($_GET['step'] == '4') : ?>
            <p>Nama : <?= $_POST['name'] ?? '' ?></p>
            <p>Umur : <?= $_POST['old'] ?? '' ?></p>
            <p>Hobi : <?= $_POST['hobby'] ?? '' ?></p>
        <?php endif; ?>
    <?php endif; ?>

</body>

</html>