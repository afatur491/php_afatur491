<?php
require "database.php";

$query = "SELECT *
FROM person AS p
    LEFT JOIN hobi AS h
    ON person_id = p.id
";

$wheres = [];

if (!empty($_GET['name'])) array_push($wheres, ['nama']);
if (!empty($_GET['address'])) array_push($wheres, ['alamat']);

foreach ($wheres as  $w => $where) {
    if ($w == 0) $query .= "where p.{$where[0]} = :{$where[0]}";
    if ($w != 0) $query .= " AND p.{$where[0]} = :{$where[0]}";
}

$prepare = $db->prepare($query);
if (!empty($_GET['name'])) $prepare->bindParam(':nama', $_GET['name']);
if (!empty($_GET['address'])) $prepare->bindParam(':alamat', $_GET['address']);

$prepare->execute();
$persons = $prepare->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soal3</title>
</head>

<body>
    <form action="/soal-a/soal3.php">

        <div>
            <label for="">Nama : </label>
            <input type="text" name="name" value="<?php $_GET['name'] ?? '' ?>">
        </div>
        <div style="margin-top: 10px">
            <label for="">Alamat : </label>
            <input type="text" name="address" value="<?php $_GET['address'] ?? '' ?>">
        </div>
        <button type="submit">Search</button>
        <a href="/soal-a/soal3.php">Reset</a>

    </form>
    <br>
    <table style="width:50%" border="1">
        <tr>
            <td>Nama</td>
            <td>Alamat</td>
            <td>Hobi</td>
        </tr>
        <?php $name = '' ?>
        <?php foreach ($persons as $person) : ?>
            <?php if ($person['nama'] != '' && $person['alamat'] != '') : ?>
                <?php $name = $person['nama'] ?>
                <tr>
                    <td><?= $person['nama'] ?></td>
                    <td><?= $person['alamat'] ?></td>
                    <td><?= $person['hobi'] ?></td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
</body>

</html>